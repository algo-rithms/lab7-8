﻿#include <iostream>
#include <vector>
#include <queue>
#include <stack>
#include <map>

const int INF = INT_MAX; // Нескінченність для непов'язаних міст

// Функція DFS (Depth-First Search) для обходу графа у глибину
void dfs(int start, std::vector<bool>& visited, const std::vector<std::vector<int>>& adjMatrix) {
    std::stack<int> stack;
    stack.push(start);

    while (!stack.empty()) {
        int node = stack.top();
        stack.pop();

        if (!visited[node]) {
            visited[node] = true;
            std::cout << "Visited city with index: " << node << std::endl;

            // Обхід сусідів поточної вершини у зворотньому порядку
            for (int i = adjMatrix.size() - 1; i >= 0; i--) {
                if (adjMatrix[node][i] != INF && !visited[i]) {
                    stack.push(i); // Додаємо сусіда до стеку для подальшого обходу
                }
            }
        }
    }
}

// Функція BFS (Breadth-First Search) для обходу графа пошуком в ширину
void bfs(int start, std::vector<bool>& visited, std::vector<std::vector<int>>& adjMatrix, std::vector<int>& distances, std::vector<int>& parent) {
    std::queue<int> queue;
    queue.push(start); // Додаємо початкову вершину до черги
    visited[start] = true;
    distances[start] = 0;
    parent[start] = -1; // Відсутність батьківської вершини для початкової вершини

    while (!queue.empty()) {
        int node = queue.front();
        queue.pop(); // Видаляємо поточну вершину з черги

        // Обхід сусідів поточної вершини
        for (int i = 0; i < adjMatrix.size(); i++) {
            if (adjMatrix[node][i] != INF && !visited[i]) {
                visited[i] = true;
                distances[i] = distances[node] + adjMatrix[node][i];
                parent[i] = node; // Встановлюємо батька для сусідньої вершини
                queue.push(i); // Додаємо сусіда до черги для подальшого обходу
            }
        }
    }
}


// Функція для виведення шляху та його довжини
void printPath(std::vector<int> parent, int j, std::vector<std::vector<int>>& adjMatrix, const std::map<int, std::string>& indexCity) {
    if (parent[j] == -1)
        return;

    printPath(parent, parent[j], adjMatrix, indexCity);
    std::cout << " -> " << indexCity.at(j) << " (" << adjMatrix[parent[j]][j] << " km)";
}

int main() {
    std::map<std::string, int> cityIndex = {
        {"Kyiv", 0}, {"Zhytomyr", 1}, {"Novohrad-Volynskyi", 2}, {"Rivne", 3}, {"Lutsk", 4},
        {"Berdychiv", 5}, {"Vinnytsia", 6}, {"Khmelnytskyi", 7}, {"Ternopil", 8},
        {"Shepetivka", 9}, {"Bila Tserkva", 10}, {"Uman", 11}, {"Cherkasy", 12}, {"Kremenchuk", 13},
        {"Poltava", 14}, {"Kharkiv", 15}, {"Pryluky", 16}, {"Sumy", 17}, {"Myrhorod", 18}
    };


    std::map<int, std::string> indexCity;
    for (const auto& pair : cityIndex) {
        indexCity[pair.second] = pair.first;
    }

    int n = cityIndex.size();
    std::vector<std::vector<int>> adjMatrix(n, std::vector<int>(n, INF));

    // Введення даних у матрицю суміжності
    adjMatrix[0][1] = adjMatrix[1][0] = 135;
    adjMatrix[1][2] = adjMatrix[2][1] = 80;
    adjMatrix[2][3] = adjMatrix[3][2] = 100;
    adjMatrix[3][4] = adjMatrix[4][3] = 68;

    adjMatrix[1][5] = adjMatrix[5][1] = 38;
    adjMatrix[5][6] = adjMatrix[6][5] = 73;
    adjMatrix[6][7] = adjMatrix[7][6] = 110;
    adjMatrix[7][8] = adjMatrix[8][7] = 104;

    adjMatrix[1][9] = adjMatrix[9][1] = 115;

    adjMatrix[0][10] = adjMatrix[10][0] = 78;
    adjMatrix[10][11] = adjMatrix[11][10] = 115;
    adjMatrix[10][12] = adjMatrix[12][10] = 146;
    adjMatrix[12][13] = adjMatrix[13][12] = 105;

    adjMatrix[10][14] = adjMatrix[14][10] = 181;
    adjMatrix[14][15] = adjMatrix[15][14] = 130;

    adjMatrix[0][16] = adjMatrix[16][0] = 128;
    adjMatrix[16][17] = adjMatrix[17][16] = 175;
    adjMatrix[16][18] = adjMatrix[18][16] = 109;

    std::vector<bool> visited(n, false);
    std::vector<int> distances(n, INF);
    std::vector<int> parent(n);

    bfs(0, visited, adjMatrix, distances, parent);

    // Вивід
    for (int i = 1; i < n; i++) {
        std::cout << "Distance from Kyiv to " << indexCity[i] << " is " << distances[i] << " km";
        std::cout << ", Route: " << indexCity[0];
        printPath(parent, i, adjMatrix, indexCity);
        std::cout << std::endl;
    }
}
